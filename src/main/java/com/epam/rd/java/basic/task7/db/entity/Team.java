package com.epam.rd.java.basic.task7.db.entity;

import java.util.Objects;

public class Team {

    private int id;

    private String name;

    public static int counter = 0;

    private int number;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public static Team createTeam(String name) {
        Team team = new Team();
        team.setName(name);
        team.setNumber(name.hashCode());
        return team;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Team team = (Team) o;
        return Objects.equals(name, team.name);
    }

    @Override
    public String toString() {
        return name;
    }


}
