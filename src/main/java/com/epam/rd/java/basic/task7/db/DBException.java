package com.epam.rd.java.basic.task7.db;

import java.sql.SQLException;

public class DBException extends Exception {
//	private String message;
//	Throwable cause;
	public DBException(String message, Throwable cause) {
		System.err.print(message + cause.getMessage());
	}

	public DBException() {
		super();
	}
	//
//
//	public DBException(String message) {
//		super(message);
//	}
//
//	public DBException(Throwable cause) {
//		super(cause);
//	}
//
//	protected DBException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
//		super(message, cause, enableSuppression, writableStackTrace);
//	}
}
